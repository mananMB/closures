const cacheFunction = (cb) => {
  const cache = {};
  return (...arguments) => {
    console.log(`\n\nIn Cache: ${`${arguments}` in cache}`);
    if (`${arguments}` in cache) {
      console.log("Fetching from cache:");
      return cache[arguments];
    }
    console.log(cache);
    console.log("Calling Function:");
    return (cache[arguments] = cb(...arguments));
  };
};

module.exports = cacheFunction;
