const counterFactory = (value = 0) => {
  return {
    increment: () => ++value,
    decrement: () => --value,
  };
};

module.exports = counterFactory;
