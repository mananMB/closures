const { expect } = require("chai");
const limitFunctionCallCount = (cb, limit) => {
  let callCount = 0;
  if (cb === undefined || typeof cb !== "function") {
    throw "Callback not provided or invalid callback provided.";
  } else if (limit === undefined) {
    throw "Limit not provided.";
  }
  return (...arguments) => {
    if (callCount++ < limit) {
      return cb(...arguments);
    }
    return null;
  };
};

module.exports = limitFunctionCallCount;
