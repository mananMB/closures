const cacheFunction = require("../cacheFunction");

const cb = (value, valueAgain, anotherValue) => {
  return value + valueAgain + anotherValue;
};
let result = cacheFunction(cb);

console.log(result(10, 20, 20));
console.log(result(10, 10, 20));
console.log(result(20, 30, 20));
console.log(result(5, 10, 20));
console.log(result(10, 20, 20));
console.log(result(52, 10, 20));
console.log(result(20, 30, 20));
