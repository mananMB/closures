const expect = require('chai').expect;
const counterFactory = require('../counterFactory');

describe('Test counterFactory',  () => {
    it('should return incremented value',  () => {
        expect(counterFactory().increment(1)).to.equal(2);
    });
    it('should return decremented value',  () => {
        expect(counterFactory().decrement(1)).to.equal(0);
    });
});