const expect = require("chai").expect;
const limitFunctionCallCount = require("../limitFunctionCallCount");

describe("Test limitFunctionCallCount", () => {
  it("should only call the function n times", () => {
    let n = 3;
    let array = [];
    let result = limitFunctionCallCount((value) => array.push(value), n);
    for (let index = 1; index <= n + 5; index++) {
      result(index);
    }
    expect(array).to.deep.equal([1, 2, 3]);
  });
  it("should return null when callCount > n or when function cannot be called", () => {
    let n = 3;
    let array = [];
    let limitCall = limitFunctionCallCount((value) => array.push(value), n);
    limitCall(1);
    limitCall(2);
    limitCall(3);
    limitCall(4);
    let result = limitCall(5);
    expect(result).to.equal(null);
  });
});
